FROM alpine:3.18 as builder
LABEL stage=go-builder
ENV GOPROXY=https://goproxy.cn,direct
WORKDIR /app/
COPY ./ ./
RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.tuna.tsinghua.edu.cn/g' /etc/apk/repositories && \
    apk add --no-cache bash curl gcc git go musl-dev && \
    bash build.sh dev docker

FROM alpine:3.18
LABEL MAINTAINER="i@nn.ci"
VOLUME /opt/alist/data/
WORKDIR /opt/alist/
COPY --from=builder /app/bin/alist ./
COPY entrypoint.sh /entrypoint.sh
RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.tuna.tsinghua.edu.cn/g' /etc/apk/repositories && \
    apk add --no-cache bash ca-certificates su-exec tzdata && \
    chmod +x /entrypoint.sh
ENV PUID=0 PGID=0 UMASK=022
ENV ALIST_ADMIN_PASSWORD=admin
EXPOSE 5244 5245
CMD [ "/entrypoint.sh" ]
